<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# meta-oniro-blueprints-eddie

TODO

Welcome to the Oniro blueprint for the EDDIE project.

Oniro is an Eclipse Foundation project focused on the development of a
distributed open source operating system for consumer devices.

*\*Oniro is a trademark of Eclipse Foundation.*

## About
The objective of the EDDIE project is to design
a middleware running on top of the operating system of the various Oniro
devices. Capable of giving to the applications and services an global view
on a unified pool of virtual resources that can be exploited for an efficient
execution of any application or service required by the user.

In this repository you can find the necessary yocto layers to run the EDDIE blueprint.

## Set up your workspace
The following instructions shows how to set up the Oniro Project workspace, please visit the [Oniro docs](https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html) for more details.
```console
$ mkdir ~/oniroproject; cd ~/oniroproject
$ repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b kirkstone
$ repo sync --no-clone-bundle
$ TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-linux
```

## Add the EDDIE blueprint layers to your build
Clone this repository into your workspace
```console 
$ git clone git@booting.oniroproject.org:distro/blueprints/eddie/meta-oniro-blueprints-eddie.git ~/oniroproject/meta-eddie
```

Add the layers to your yocto build
```console
$ bitbake-layers add-layer ~/oniroproject/meta-eddie/meta-oniro-blueprints-eddie
$ bitbake-layers add-layer ~/oniroproject/meta-eddie/meta-oniro-blueprints-core
```

## Build and run a linux target

Build the Oniro image with the EDDIE blueprint 
```console
$ DISTRO="oniro-linux-blueprint-eddie" MACHINE=<your board> bitbake blueprint-eddie-image
```

- If you want to run EDDIE on a Qemu target:
```console
$ runqemu blueprint-eddie-image wic ovmf nographic
```
- If you want to run EDDIE on an actual board, such as a Raspberry Pi, please specify the correct MACHINE variable and follow the [docs](https://docs.oniroproject.org/en/latest/oniro/hardware-support/boards/raspberrypi4.html) for more information on how to flash your board.

After the booting sequence you will find `eddie-server` and `eddie-client` binaries. The `eddie-server` service will automatically run as a systemd service.

For further information see the [EDDIE project main repository](https://booting.oniroproject.org/antonio.miele/eddie).

## Build and run a zephyr target
Build a zephyr image for the arduino-nano-33-ble board
```console
$ DISTRO="oniro-zephyr" MACHINE="arduino-nano-33-ble" bitbake zephyr-blueprint-eddie
```

Flash the board appending `-c flash_usb` to the previous command:
```console
$ DISTRO="oniro-zephyr" MACHINE="arduino-nano-33-ble" bitbake zephyr-blueprint-eddie -c flash_usb
```

For more detailed instructions visit the [Oniro docs](https://docs.oniroproject.org/en/latest/oniro/hardware-support/boards/arduino-nano-33-ble.html).

[Here](https://gitlab.eclipse.org/mrfrank/eddie-zephyr) you can find an alternative way to build and flash your zephyr target and further information on the zephyr node.
