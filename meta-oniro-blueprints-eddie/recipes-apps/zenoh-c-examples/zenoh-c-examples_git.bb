# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SUMMARY = "Zenoh C examples"
DESCRIPTION = "Examples imported from zenoh-c showing usage of the Zenoh C client library"
LICENSE = "Apache-2.0"

SRC_OPT_PROTO = "protocol=https"
SRC_OPT_BRANCH = "branch=main"
SRC_OPT_CLONE_DIR = "git/apps/zenoh-c-examples"
SRC_OPT_DEST = "destsuffix=${SRC_OPT_CLONE_DIR}"

SRC_OPTIONS = "${SRC_OPT_PROTO};${SRC_OPT_DEST};${SRC_OPT_BRANCH}"
SRC_URI += "git://booting.oniroproject.org/francesco.pham/zenoh-c-client-example.git;${SRC_OPTIONS}"

SRCREV = "43465198e8c9c21056ad734ee0f15ea0a5badaf8"

S = "${WORKDIR}/${SRC_OPT_CLONE_DIR}"

LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=ba963850f6731c74878fe839d227e675"

inherit cmake pkgconfig

DEPENDS:append = "\
	zenoh-c \
    "

RDEPENDS:${PN}:append = "\
	zenoh-c \
    "