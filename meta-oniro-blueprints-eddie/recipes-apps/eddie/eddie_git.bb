# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SUMMARY = "Eddie blueprint: enabling distributed intelligence in Oniro"
DESCRIPTION = "Enabling distributed intelligence in Oniro"
LICENSE = "Apache-2.0"

LIC_FILES_CHKSUM = "file://LICENSES/Apache-2.0.txt;md5=ef3dabb8f39493f4ea410bebc1d01755"

DEPENDS += "coap"

SRC_URI += "git://gitlab.eclipse.org/eclipse/oniro-core/eddie.git;protocol=https;branch=main \
            file://eddie-server.service"

SRCREV = "9ba361241aabb6fe383e44e64b9f16d3f3740a63"

S = "${WORKDIR}/git"

inherit cmake pkgconfig systemd

SYSTEMD_SERVICE:${PN} = "eddie-server.service"

do_install:append() {
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/eddie-server.service ${D}${systemd_unitdir}/system/
}
